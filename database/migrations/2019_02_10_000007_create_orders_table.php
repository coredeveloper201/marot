<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->bigInteger('product_id');
            $table->string('order_code');
            $table->text('comments')->nullable();
            $table->bigInteger('location');
            $table->string('units')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1.In progress, 2.In print 3.delivered');
            $table->tinyInteger('visibility')->default(1)->comment('1.Published, 2.Draft');
            $table->tinyInteger('is_active')->default(1)->comment('1.active, 2.inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
