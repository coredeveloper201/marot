<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            'idClient' => 1,
            'name' => "Location A",
            'postalCode' => 8911,
            'detail' => "Location A- Barcelona - 08911 - Central Office",
            "created_at" => Carbon::now()
        ]);
        DB::table('locations')->insert([
            'idClient' => 1,
            'name' => "Location B",
            'postalCode' => 8923,
            'detail' => "Location B- Madrid - 08923 - Central Office",
            "created_at" => Carbon::now()
        ]);
        DB::table('locations')->insert([
            'idClient' => 2,
            'name' => "Location A",
            'postalCode' => 8312,
            'detail' => "Location A - lOREM IPSUM",
            "created_at" => Carbon::now()
        ]);
    }
}
