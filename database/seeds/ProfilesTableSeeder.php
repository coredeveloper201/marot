<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert(['name' => "Admin", "created_at" => Carbon::now()]);
        DB::table('profiles')->insert(['name' => "Advanced", "created_at" => Carbon::now()]);
        DB::table('profiles')->insert(['name' => "Basic", "created_at" => Carbon::now()]);
    }
}
