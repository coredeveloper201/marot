<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert(['name' => "Company A", "logo" => "https://www.crearlogogratisonline.com/images/crearlogogratis_1024x1024_01.png", "created_at" => Carbon::now()]);
        DB::table('clients')->insert(['name' => "Company B", "logo" => "https://es.freelogodesign.org/Content/img/logo-ex-7.png", "created_at" => Carbon::now()]);
    }
}
