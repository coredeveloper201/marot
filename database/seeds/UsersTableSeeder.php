<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Administrator",
            'phone' => "933835003",
            'user_type' => 1,
            'email' => "clients@marot.cat",
            'password' => bcrypt('123asd123'),
            "created_at" => Carbon::now()
        ]);
    }
}
