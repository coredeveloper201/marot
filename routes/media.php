<?php

Route::prefix('media')->group(function () {
    Route::get('/image', 'Media\MediaController@ImagePreview')->name('media.image.preview');
    Route::get('/local', 'Media\MediaController@ImagePreviewlocal')->name('media.image.preview.local');
    Route::post('/', 'Media\MediaController@Media_add')->name('media.add');
});
