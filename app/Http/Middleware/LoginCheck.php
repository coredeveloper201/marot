<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LoginCheck
{
    public function handle($request, Closure $next)
    {
        if(Auth::guard('admins')->check()) {
            return redirect()->route('admin.orders');
        }
        return $next($request);
    }
}
