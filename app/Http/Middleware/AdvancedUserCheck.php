<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdvancedUserCheck
{
    public function handle($request, Closure $next)
    {
        if(!(Auth::guard('admins')->user()['user_type'] == 2)) {
            return redirect()->route('admin.orders');
        }
        return $next($request);
    }
}
