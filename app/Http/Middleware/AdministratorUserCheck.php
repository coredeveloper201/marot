<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdministratorUserCheck
{
    public function handle($request, Closure $next)
    {
        if(!(Auth::guard('admins')->user()['user_type'] == 1)) {
            return redirect()->route('admin.orders');
        }
        return $next($request);
    }
}
