<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Repository\AdminRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class AdminController extends BaseController
{
    /*================*/
    // Authentication
    /*================*/
    public function ClientsGetAll(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->ClientsGetAll($request);
        return response()->json($rv, 200);
    }

    public function ProfilesGetAll(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->ProfilesGetAll($request);
        return response()->json($rv, 200);
    }

    public function LocationsGetAll(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->LocationsGetAll($request);
        return response()->json($rv, 200);
    }

    /*============*/
    // User
    /*============*/
    public function UserCreate(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->UserCreate($request);
        return response()->json($rv, 200);
    }

    public function UserEdit(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->UserEdit($request);
        return response()->json($rv, 200);
    }

    public function UserGetAll(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->UserGetAll($request);
        return response()->json($rv, 200);
    }

    public function UserGetSingle(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->UserGetSingle($request);
        return response()->json($rv, 200);
    }

    public function UserDelete(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->UserDelete($request);
        return response()->json($rv, 200);
    }

    /*============*/
    // User Profile
    /*============*/
    public function UserGetInfo(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->UserGetInfo($request);
        return response()->json($rv, 200);
    }

    public function UserUpdateInfo(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->UserUpdateInfo($request);
        return response()->json($rv, 200);
    }

    /*============*/
    // Products
    /*============*/
    public function ProductCreate(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->ProductCreate($request);
        return response()->json($rv, 200);
    }

    public function ProductEdit(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->ProductEdit($request);
        return response()->json($rv, 200);
    }

    public function ProductGetAll(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->ProductGetAll($request);
        return response()->json($rv, 200);
    }

    public function ProductGetSingle(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->ProductGetSingle($request);
        return response()->json($rv, 200);
    }

    public function ProductDelete(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->ProductDelete($request);
        return response()->json($rv, 200);
    }

    /*============*/
    // Orders
    /*============*/
    public function OrderCreate(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->OrderCreate($request);
        return response()->json($rv, 200);
    }

    public function OrderEdit(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->OrderEdit($request);
        return response()->json($rv, 200);
    }

    public function OrderGetAll(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->OrderGetAll($request);
        return response()->json($rv, 200);
    }

    public function OrderGetSingle(Request $request)
    {
        $adminRepo = new AdminRepository();
        $rv = $adminRepo->OrderGetSingle($request);
        return response()->json($rv, 200);
    }
}
