<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu"class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  m-menu__item {{$nav == 'Orders' ? 'link-active' : ''}}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a href="{{route('admin.orders')}}" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-truck w-link"></i>
                    <span class="m-menu__link-text w-link">Orders</span>
                </a>
            </li>
            @if($Auth['user_type'] == 1)
            <li class="m-menu__item  m-menu__item {{$nav == 'Users' ? 'link-active' : ''}}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a href="{{route('admin.users')}}" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-users w-link"></i>
                    <span class="m-menu__link-text w-link">Users</span>
                </a>
            </li>
            @endif
            <li class="m-menu__item  m-menu__item {{$nav == 'Products' ? 'link-active' : ''}}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a href="{{route('admin.products')}}" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-layers w-link"></i>
                    <span class="m-menu__link-text w-link">Products</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>