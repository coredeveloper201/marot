<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        Marot | {{$page}}
    </title>

    <meta name="title" content="Marot">
    <meta name="og:title" content="Marot">
    <meta name="image" content="{{env('ASSETS_PATH')}}/image/logo_m.png">
    <meta name="og:image" content="{{env('ASSETS_PATH')}}/image/logo_m.png">

    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    {{--<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>--}}

    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->
    <link href="{{env('ASSETS_PATH')}}/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
    <link href="{{env('ASSETS_PATH')}}/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{env('ASSETS_PATH')}}/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />



    <script src="{{env('ASSETS_PATH')}}/plugins/jQuery/jquery-3.3.1.min.js" type="application/javascript"></script>
    <script src="{{env('ASSETS_PATH')}}/plugins/vue/vue.min.js" type="application/javascript"></script>
    <script src="https://cdn.polyfill.io/v2/polyfill.js?features=es6" type="application/javascript"></script>

    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{env('ASSETS_PATH')}}/image/favicon.png" />
    <link href="{{env('ASSETS_PATH')}}/stylesheets/marot.min.css" rel="stylesheet" type="text/css" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="{{env('ASSETS_PATH')}}/custom.css" rel="stylesheet" type="text/css" />

    @yield('resources')
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page marot">

    <!-- BEGIN: Header -->
    @include('global.header')
    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>

        @include('global.sidenav')
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            @yield('content')
        </div>

    </div>
    <!-- end:: Body -->

    <!-- begin::Footer -->
    <footer class="m-grid__item		m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
						{{--	<span class="m-footer__copyright">
								2019 &copy; Metronic theme by
								<a href="https://keenthemes.com" class="m-link">
									Keenthemes
								</a>
							</span>--}}
                </div>
            </div>
        </div>
    </footer>
    <!-- end::Footer -->
</div>
<!-- end:: Page -->

<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->		    <!-- begin::Quick Nav -->



<!--begin::Base Scripts -->
<script src="{{env('ASSETS_PATH')}}/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="{{env('ASSETS_PATH')}}/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Vendors -->
<script src="{{env('ASSETS_PATH')}}/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{env('ASSETS_PATH')}}/app/js/dashboard.js" type="text/javascript"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--end::Page Snippets -->
</body>
<!-- end::Body -->
</html>
