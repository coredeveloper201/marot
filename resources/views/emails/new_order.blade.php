<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Activation Success</title>
</head>
<body>

<div class="mail-templete">
    <div class="mail-header"
         style="width: 100%; height: 70px; line-height: 70px; background-image: url('{{env('ASSETS_PATH')}}/image/header_mail.png');
                 background-color: #E40425; color: #fff; font-size: 30px; background-position: center; background-size: contain; background-repeat: no-repeat;
                 text-align: center;">
    </div>
    <div class="mail-wrapper" style="width: 100%; height: auto; display: inline-block; box-sizing: border-box; padding: 30px 0px; text-align: center; font-size: 16px">
        <div class="inner-wrapper" style="width: 600px; margin: 0 auto">
            <strong style="font-size: 14px">Hola {{$mailInfo['user']['email']}} ({{$mailInfo['user']['name']}}) le adjuntamos la información <br> del pedido realizado: <br></strong>
            <br>
            <br>
            <span>
                <strong>Date:</strong> <strong><span style="color: #E40425">{{$mailInfo['order']['formatted_date']}}</span></strong>
            </span>
            <br>
            <span>
                <strong>Product code:</strong> <strong><span style="color: #E40425">{{$mailInfo['product']['code_product']}}</span></strong><br>
            </span>
            <span>
                <strong>Product Name:</strong> <strong><span style="color: #E40425">{{$mailInfo['product']['name']}}</span></strong><br>
            </span>
            <span>
                <strong>Product Tags:</strong> <strong style="color: #E40425">{{$mailInfo['order']['tags']}}</strong> <br>
            </span>
            <span>
                <strong>Quantity:</strong> <strong><span style="color: #E40425">{{$mailInfo['order']['units']}}</span></strong><br>
            </span>
            <span>
                <strong>Final Price:</strong> <strong><span style="color: #E40425">{{$mailInfo['final_price']}}&euro;</span></strong><br>
            </span>
            <br>
            <span>
                <strong>Order code:</strong> <strong>{{ ($mailInfo['order']['order_code'] == '') ? '(si no lo ha ingresado, edite el pedido cuando lo tenga e introdúzcalo)' : $mailInfo['order']['order_code'] }}</strong><br>
            </span>
            {{-- (si no lo ha ingresado, edite el pedido cuando lo tenga e introdúzcalo) --}}
            <span>
                <strong>Location:</strong> <strong style="color: #E40425">{{$mailInfo['order']['location_address']}}</strong> <br>
            </span>
            <span>
                <strong>Comment:</strong> <strong style="color: #E40425">{{$mailInfo['order']['comments']}}</strong> <br>
            </span>
            <br>
            {{-- @if(isset($mailInfo['order']['comments']))
                <span style="color: #E40425">
                    <srtong>*Comments:</srtong>
                    <br>
                    {{$mailInfo['order']['comments']}}
                    <br>
                </span>
            <br>
            <br>
            @endif --}}
            {{-- Para cualquier consulta le proporcionamos un mail de contacto <br> --}}
            {{-- <span style="color: #E40425">clients@marot.cat</span> y un número de teléfono <span style="color: #E40425">933 835 003</span> --}}
            {{-- <br> --}}
        </div>
    </div>
    <br>
    <br>
    <div class="main-footer"
         style="width: 100%; height: 80px; line-height: 70px; background-image: url('{{env('ASSETS_PATH')}}/image/footer_mail.png');
                 background-color: #fff; color: #fff; font-size: 30px; background-position: center; background-size: contain; background-repeat: no-repeat;
                 text-align: center;">
    </div>
</div>

</body>
</html>