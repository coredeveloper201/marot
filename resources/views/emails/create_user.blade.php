<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{--<title>Activation Success</title>--}}
</head>
<body>
<div class="mail-templete">
    <div class="mail-header"
         style="width: 100%; height: 70px; line-height: 70px; background-image: url('{{env('ASSETS_PATH')}}/image/header_mail.png');
                 background-color: #E40425; color: #fff; font-size: 30px; background-position: center; background-size: contain; background-repeat: no-repeat;
                 text-align: center;">
    </div>
    <div class="mail-wrapper" style="width: 100%; height: auto; display: inline-block; box-sizing: border-box; padding: 30px 50px;">
        <div class="inner-wrapper" style="display: inline-block; text-align: center; width: 100%; font-size: 20px !important;">
            Bienvenido a nuestro portal de pedidos. <br>
            Para ingresar a la plataforma debe acceder a este enlace<br>
            con los siguientes datos:
            <br>
            <br>
            <a target="_blank" rel="noopener noreferrer" href="http://clients.impremtamarot.com" style="color: #E40425">
                <strong>clients.impremtamarot.com</strong>
            </a><br>
            <span>nombre:  <span style="color: #E40425"><strong>{{$user['email']}}</strong></span></span><br>
            <span>contraseña <span style="color: #E40425"><strong>{{$user['new_password']}}</strong></span></span><br>
            <br>
            <br>
            <br>
            Para cualquier consulta nos puede escribir a este correo electrónico <br>
            <span style="color: #E40425"><strong>clients@marot.cat</strong></span> o llamar-nos al <span style="color: #E40425"><strong>93 383 50 03</strong></span>
        </div>
    </div>
    <br>
    <br>
    <div class="main-footer"
         style="width: 100%; height: 80px; line-height: 70px; background-image: url('{{env('ASSETS_PATH')}}/image/footer_mail.png');
                 background-color: #fff; color: #fff; font-size: 30px; background-position: center; background-size: contain; background-repeat: no-repeat;
                 text-align: center;">
    </div>
</div>

</body>
</html>