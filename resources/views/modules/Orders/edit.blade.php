@extends('layouts.structure')

@section('resources')

@endsection

@section('content')
    <div class="m-content" id="Vue_component_main">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
											<span class="m-portlet__head-icon m--hide">
												<i class="la la-gear"></i>
											</span>
                                <h3 class="m-portlet__head-text">
                                   Ordered details
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                        <div class="m-form m-form--fit m-form--label-align-right">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group inline-text">
                                            <span>
                                                <span>
                                                    <strong>ID: &nbsp;</strong>
                                                </span>
                                                <span v-text="ProductDetails.id"></span>
                                            </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                            <span>
                                                <span>
                                                    <strong>Product name: &nbsp;</strong>
                                                </span>
                                                <span v-text="ProductDetails.product_name"></span>
                                            </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                            <span>
                                                <span>
                                                    <strong>Code Product: &nbsp;</strong>
                                                </span>
                                                <span v-text="ProductDetails.code_product"></span>
                                            </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                    <span>
                                        <span>
                                            <strong>Units: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.units"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                    <span>
                                        <span>
                                            <strong>User mail: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.user_email"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                    <span>
                                        <span>
                                            <strong>Phone: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.user_phone"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                    <span>
                                        <span>
                                            <strong>Client: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.client_name"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                    <span>
                                        <span>
                                            <strong>Location: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.address"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                    <span>
                                        <span>
                                            <strong>Date: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.formatted_date"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group inline-text">
                                    <span>
                                        <span>
                                            <strong>Comments: &nbsp;</strong>
                                        </span>
                                        <br>
                                        <span v-text="ProductDetails.comments"></span>
                                    </span>
                                </div>
                                <form @submit.prevent="UpdateTicket">
                                    @if($Auth['user_type'] == 1)
                                        <div class="form-group m-form__group">
                                        <span>
                                            <span>
                                                <strong>Ticket status: &nbsp;</strong>
                                            </span>
                                            <br>
                                            <span class="btn-group-marot">
                                                <span class="margin-5"></span>
                                                <button class="btn btn-metal m-btn btn-sm m-btn--pill status-btn"
                                                        @click="TicketForm.status = 1" type="button"
                                                        :class="{'in_process': TicketForm.status === 1}">
                                                    In Progress
                                                </button>
                                                <button class="btn btn-metal m-btn btn-sm m-btn--pill status-btn"
                                                        @click="TicketForm.status = 2" type="button"
                                                        :class="{'in_print': TicketForm.status === 2}">
                                                    In Print
                                                </button>
                                                <button class="btn btn-metal m-btn btn-sm m-btn--pill status-btn"
                                                        @click="TicketForm.status = 3" type="button"
                                                        :class="{'delivered': TicketForm.status === 3}">
                                                    Delivered
                                                </button>
                                            </span>
                                        </span>
                                    </div>
                                        <div class="form-group m-form__group">
                                        <span>
                                            <span>
                                                <strong>Visibility: &nbsp;</strong>
                                            </span>
                                            <br>
                                            <span class="btn-group-marot">
                                                <span class="margin-5"></span>
                                                <button class="btn btn-metal m-btn btn-sm m-btn--pill status-btn"
                                                        @click="TicketForm.visibility = 1" type="button"
                                                        :class="{'active-btn': TicketForm.visibility === 1}">
                                                    Published
                                                </button>
                                                <button class="btn btn-metal m-btn btn-sm m-btn--pill status-btn"
                                                        @click="TicketForm.visibility = 2" type="button"
                                                        :class="{'active-btn': TicketForm.visibility === 2}">
                                                    Draft
                                                </button>
                                            </span>
                                        </span>
                                    </div>
                                    @endif
                                    <div class="form-group m-form__group">
                                        <div>
                                            <label>
                                                <strong>Order Code: &nbsp;</strong>
                                            </label>
                                            <div class="row">
                                                <div class="col-md-6 has-danger">
                                                    <input v-model="TicketForm.order_code" name="order_code" type="text" class="form-control" placeholder="Order code">
                                                    <div class="form-control-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group m-form__group text-right">
                                        <a href="{{route('admin.orders')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn gray">
                                            <span>
                                                <i class="fa fa-times-circle"></i>
                                                <span>Cancel</span>
                                            </span>
                                        </a>
                                        <button type="submit"
                                                class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn"
                                                :disabled="HttpRequest === true"
                                                :class="{'m-loader m-loader--right m-loader--light': HttpRequest === true}">
                                            <span>
                                                <i class="fa fa-check-circle"></i>
                                                <span>Save</span>
                                            </span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                ProductDetails: {},
                HttpRequest: false,
                order_id: '{{$order_id}}',
                TicketForm: {
                    id: '',
                    status: '',
                    visibility: '',
                    order_code: '',
                }
            },
            methods: {
                GetDetails: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path + "/order/single",
                        type: "get",
                        data: {id: _this.order_id},
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.ProductDetails = response.data;
                                _this.TicketForm.id = response.data.id;
                                _this.TicketForm.status = response.data.status;
                                _this.TicketForm.visibility = response.data.visibility;
                                _this.TicketForm.order_code = response.data.order_code;
                            }
                        }
                    });
                },
                UpdateTicket: function () {
                    let _this = this;
                    _this.HttpRequest = true;
                    $('.form-control-feedback').html('');
                    $.ajax({
                        url: _this.Api_path + "/order",
                        type: "put",
                        data: _this.TicketForm,
                        success: function (response) {
                            _this.HttpRequest = false;
                            if (response.status === 2000) {
                                console.log(response);
                                window.location.href = '{{route('admin.orders')}}';
                            } else {
                                _this.ErrorHandaler(response.error)
                            }
                        }
                    });
                },
                ErrorHandaler: function (errors) {
                    $.each(errors, function (i, v) {
                        $('[name=' + i + ']').closest('.has-danger').find('.form-control-feedback').html(v);
                    });
                }
            },
            created() {
                this.GetDetails();
            }
        });
    </script>
@endsection