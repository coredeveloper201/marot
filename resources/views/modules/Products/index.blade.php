@extends('layouts.structure')

@section('resources')

@endsection

@section('content')
    <div class="m-content" id="Vue_component_main">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    {{$page}}
                                </h3>
                            </div>
                        </div>
                        <div class="col-md-7 text-right">
                            <div class="row">
                                <div class="col-md-6">
                                    @if($Auth['user_type'] == 1)
                                        <a href="{{route('admin.products.create')}}" style="padding: -0.25rem 0.8rem"
                                           class="btn btn-primary m-btn m-btn--custom btn-sm m-btn--icon m-btn--pill m-btn--air ma_btn">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>Create New Product</span>
                                            </span>
                                        </a>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <input @keyup="Search" v-model="GetConfig.keyword" type="text"
                                        class="form-control m-input m-input--square"
                                        placeholder="Search here...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table table-striped m-table">
                            <thead>
                                <tr>
                                    <th>
                                        <div @click="filterData('code_product')" class="pointer">
                                            <span>Code Product</span>&nbsp;
                                            <span><i class="fa fa-sort"></i></span>
                                        </div>
                                    </th>
                                    <th>
                                        <div @click="filterData('name')" class="pointer">
                                            <span>Product</span>&nbsp;
                                            <span><i class="fa fa-sort"></i></span>
                                        </div>
                                    </th>
                                    @if($Auth['user_type'] == 1)
                                        <th>
                                            <div @click="filterData('clients')" class="pointer">
                                                <span>Client</span>&nbsp;
                                                <span><i class="fa fa-sort"></i></span>
                                            </div>
                                        </th>
                                    @endif
                                    <th class="text-right"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr v-if="Products.length !== 0" v-for="Product in Products">
                                <td v-text="Product.code_product"></td>
                                <td v-text="Product.name"></td>
                                @if($Auth['user_type'] == 1)
                                    <td v-text="Product.client_name"></td>
                                @endif
                                <td class="text-right">
                                    @if($Auth['user_type'] == 1)
                                        <a :href="'{{env('ROUTE_URL')}}/products/edit/'+Product.id"
                                           class="btn btn-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill ma_btn info">
                                            <i class="la la-pencil"></i>
                                        </a>
                                        <button @click="DeleteItem(Product.id)"
                                                class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--custom m-btn--pill">
                                            <i class="la la-trash"></i>
                                        </button>
                                    @else
                                        <a :href="'{{env('ROUTE_URL')}}/products/view/'+Product.id"
                                           class="btn btn-danger btn-sm m-btn m-btn--custom m-btn--pill">
                                            <span>Make a ticket</span>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            <tr v-if="Products.length == 0">
                                <td colspan="4">
                                    <div class="m-alert m-alert--outline alert alert-success alert-dismissible fade show text-center" role="alert">
                                        Products list is empty.
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="pagination" v-if="GetConfig.pageCount > 1">
                        <button @click="navPagination(1)"
                                class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only"
                                :disabled="GetConfig.pageNo == 1">
                            <i class="la la-angle-left"></i>
                        </button>
                        <button v-for="count in GetConfig.pageCount"
                                class="btn btn-metal btn-sm m-btn  m-btn m-btn--icon" style="margin: 0 3px;"
                                :class="{'ma_btn': count == GetConfig.pageNo}" @click="goToPage(count)">
                            <span v-text="count"></span>
                        </button>
                        <button @click="navPagination(2)"
                                class="btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only"
                                :disabled="GetConfig.pageNo == GetConfig.pageCount">
                            <i class="la la-angle-right"></i>
                        </button>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Form-->
        </div>
    </div>
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                Products: [],
                GetConfig: {
                    pageNo: 1,
                    limit: 15,
                    keyword: '',
                    filter: '',
                    filter_type: '',
                    pageCount: 0,
                }
            },
            methods: {
                GetProducts: function () {
                    let _this = this;
                    _this.GetConfig.pageCount = 0;
                    $.ajax({
                        url: _this.Api_path + "/products",
                        type: "get",
                        data: _this.GetConfig,
                        success: function (response) {
                            _this.Products = response.data;
                            _this.GetConfig.pageCount = Math.ceil(response.totalData / _this.GetConfig.limit);
                        }
                    });
                },
                goToPage: function (pageNo) {
                    this.GetConfig.pageNo = pageNo;
                    this.GetProducts();
                },
                navPagination: function (type) {
                    if (type === 1) {
                        if (this.GetConfig.pageNo !== 1) {
                            this.GetConfig.pageNo--;
                        }
                    } else {
                        if (this.GetConfig.pageNo !== this.GetConfig.pageCount) {
                            this.GetConfig.pageNo++;
                        }
                    }
                    this.GetProducts();
                },
                Search: function (pageNo) {
                    this.GetConfig.pageNo = 1;
                    this.GetProducts();
                },
                filterData: function (data) {
                    this.GetConfig.filter = data;
                    if (this.GetConfig.filter_type.length === 0) {
                        this.GetConfig.filter_type = 'DESC';
                    } else if (this.GetConfig.filter_type === 'DESC') {
                        this.GetConfig.filter_type = 'ASC';
                    } else if (this.GetConfig.filter_type === 'ASC') {
                        this.GetConfig.filter_type = 'DESC';
                    }
                    this.GetProducts();
                },
                DeleteItem: function (user_id) {
                    let _this = this;
                    swal({
                        title: "Are you sure?",
                        text: "Once Removed, you will not be able to recover this product!",
                        icon: "warning",
                        buttons: ['Cancel', 'Remove'],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                url: _this.Api_path + "/products",
                                type: "delete",
                                data: {id: user_id},
                                success: function (response) {
                                    if (response.status === 2000) {
                                        _this.GetProducts();
                                        swal("Product has been Removed successfully!", {
                                            icon: "success",
                                        });
                                    }
                                }
                            });
                        } else {
                            swal("Product is safe!", {
                                icon: "info",
                            });
                        }
                    });
                }
            },
            created() {
                this.GetProducts();
            }
        });
    </script>
@endsection