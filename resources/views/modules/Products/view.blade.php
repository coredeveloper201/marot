@extends('layouts.structure')

@section('resources')
<link rel="stylesheet" href="{{ env('ASSETS_PATH') }}/alertify/css/alertify.min.css" type="text/css" />
@endsection

@section('content')
    <div class="m-content" id="Vue_component_main">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Product
                        </h3>
                    </div>
                </div>
            </div>
            <!--begin::Form-->
            <form @submit.prevent="MakeTicket">
                <div class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <span>
                                        <span>
                                            <strong>Product name: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.name"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group">
                                    <span>
                                        <span>
                                            <strong>Code Product: &nbsp;</strong>
                                        </span>
                                        <span v-text="ProductDetails.code_product"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group">
                                    <span>
                                        <span>
                                            <strong>Product Description: &nbsp;</strong>
                                        </span>
                                        <br>
                                        <span v-html="ProductDetails.description"></span>
                                    </span>
                                </div>
                                <div class="col-md-12">&nbsp&nbsp&nbsp&nbsp -</div>
                                <div class="form-group m-form__group">
                                    <span>
                                        <span>
                                            <strong>Information Order: &nbsp;</strong>
                                        </span>
                                        <br>
                                        <span v-html="ProductDetails.information_order"></span>
                                    </span>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        <strong>Quantity: &nbsp;</strong>
                                    </label>
                                    <div class="col-9 has-danger">
                                        <input v-model="TicketForm.units" @keyup="calculatePrice" @change="calculatePrice" type="number" class="form-control" placeholder="Quantity" :name="'units'">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        <strong>Final Price: &nbsp;</strong>
                                    </label>
                                    <div class="col-9 has-danger">
                                        <label v-text='calculateFinalPrice + " &euro;"'></label>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        <strong>Product Tags: &nbsp;</strong>
                                    </label>
                                    <div class="col-9 has-danger">
                                        <select name="producttags" id="producttags" v-model="TicketForm.producttags" class="form-control">
                                            <option v-for="(productTag, index) in product_tags" :value="productTag" v-text="productTag"></option>
                                        </select>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">&nbsp&nbsp&nbsp&nbsp -</div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Order code
                                    </label>
                                    <div class="col-9 has-danger">
                                        <input v-model="TicketForm.order_code" type="text" class="form-control" placeholder="Order code" :name="'order_code'">
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">&nbsp&nbsp&nbsp&nbsp -</div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-3 col-form-label text-left">
                                        Sent to
                                    </label>
                                    <div class="col-9 has-danger">
                                        <select v-model="TicketForm.location" name="location" class="form-control m-input">
                                            <option value="">Select location</option>
                                            <option v-for="location in locations" :value="location.id" v-text="location.name"></option>
                                        </select>
                                        <div class="form-control-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">&nbsp&nbsp&nbsp&nbsp -</div>
                                <div class="form-group m-form__group row has-danger">
                                    <textarea v-model="TicketForm.comments" name="comments" class="form-control" placeholder="Write here your comments"></textarea>
                                    <div class="form-control-feedback"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <div v-if="ProductDetails.file_path.length !== 0" class="square-box preview">
                                        <div class="square-content" :style="{ backgroundImage: 'url({{env('STORAGE_URL')}}image/' + ProductDetails.file_path + ')' }"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="margin-20"></div>
                        <div class="margin-20"></div>
                        <div class="form-group m-form__group text-right">
                            <a href="{{route('admin.products')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn gray">
                            <span>
                                <i class="fa fa-times-circle"></i>
                                <span>Cancel</span>
                            </span>
                            </a>
                            {{-- <button type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn"
                                :disabled="HttpRequest === true"
                                :class="{'m-loader m-loader--right m-loader--light': HttpRequest === true}">
                                <span>
                                    <i class="fa fa-check-circle"></i>
                                    <span>Make Ticket</span>
                                </span>
                            </button> --}}
                            <button type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air ma_btn"
                                :class="{'m-loader m-loader--right m-loader--light': HttpRequest === true}">
                                <span>
                                    <i class="fa fa-check-circle"></i>
                                    <span>Make Ticket</span>
                                </span>
                            </button>
                        </div>
                        <hr>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
    </div>
    <script src="{{ env('ASSETS_PATH') }}/alertify/alertify.min.js"></script>
    <script>
        new Vue({
            el: '#Vue_component_main',
            data: {
                Api_path: '{{env('API_PATH')}}',
                ProductDetails: {
                    id: '',
                    name: '',
                    code_product: '',
                    description: '',
                    information_order: '',
                    minimum_orders: '',
                    client_id: '',
                    file_path: '',
                },
                HttpRequest: false,
                locations: [],
                product_id: '{{$product_id}}',
                TicketForm: {
                    product_id: '{{$product_id}}',
                    order_code: '',
                    units: 0,
                    unit_price: 0,
                    location: 0,
                    location_address: '',
                    comments: '',
                    producttags: ''
                },
                product_tags: [],
                final_price: 0,
                quantityPriceData: [],
                calculateFinalPrice: 0
            },
            methods: {
                GetDetails: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path+"/products/single",
                        type: "get",
                        data: {id: _this.product_id},
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.ProductDetails.id = response.data.id;
                                _this.ProductDetails.name = response.data.name;
                                _this.ProductDetails.code_product = response.data.code_product;
                                _this.ProductDetails.description = response.data.description;
                                _this.ProductDetails.information_order = response.data.information_order;
                                // _this.ProductDetails.minimum_orders = response.data.minimum_orders;
                                _this.ProductDetails.minimum_orders = 0;
                                _this.TicketForm.units = response.data.minimum_orders;
                                _this.ProductDetails.client_id = response.data.client_id;
                                _this.ProductDetails.file_path = response.data.file_path;
                                _this.quantityPriceData = response.quantityPricesData;
                                let prodTags = response.data.tags.split('\n');
                                for ( let i = 0; i < prodTags.length; i++ ) {
                                    _this.product_tags.push(prodTags[i]);
                                }
                                _this.TicketForm.producttags = _this.product_tags[0];
                            }
                        }
                    });
                },
                GetLocations: function () {
                    let _this = this;
                    $.ajax({
                        url: _this.Api_path + "/locations",
                        type: "get",
                        success: function (response) {
                            if (response.status === 2000) {
                                _this.locations = response.data;
                            }
                        }
                    });
                },
                MakeTicket: function () {
                    let _this = this;
                    _this.HttpRequest = true;
                    $('.form-control-feedback').html('');
                    if(_this.TicketForm.location !== 0){
                        _this.takeLocationString(_this.TicketForm.location);
                    }
                    if ( _this.calculateFinalPrice > 0 ) {
                        $.ajax({
                            url: _this.Api_path+"/order",
                            type: "post",
                            data: _this.TicketForm,
                            success: function (response) {
                                _this.HttpRequest = false;
                                if (response.status === 2000) {
                                    window.location.href = '{{route('admin.orders')}}';
                                }else{
                                    alertify.set('notifier', 'position', 'top-right');
                                    alertify.error(response.error);
                                    _this.ErrorHandaler(response.error)
                                }
                            }
                        });
                    }
                    else {
                        alertify.set('notifier', 'position', 'top-right');
                        alertify.error('Final price cannot be zero!');
                        return false;
                    }
                },
                ErrorHandaler: function (errors) {
                    $.each(errors, function (i, v) {
                        if(v[0] === 'The selected units is invalid.'){
                            v[0] = 'Quantity can not be 0!'
                        }
                        console.log($('[name='+i+']'));
                        $('[name='+i+']').closest('.has-danger').find('.form-control-feedback').html(v);
                    });
                },
                takeLocationString: function (id) {
                    let location = this.locations.filter(x => x.id === id)[0];
                    this.TicketForm.location_address = location.name;
                },
                calculatePrice: function() {
                    let userUnit = parseInt(this.TicketForm.units);
                    let qtyPriceData = this.quantityPriceData;
                    let outOfRangeFlag = false;
                    if ( ! isNaN(userUnit) ) {
                        for ( let itemData in qtyPriceData ) {
                            if ( userUnit <= qtyPriceData[itemData].quantity ) {
                                this.calculateFinalPrice = userUnit * qtyPriceData[itemData].price;
                                this.TicketForm.unit_price = qtyPriceData[itemData].price;
                                outOfRangeFlag = false;
                                break;
                            }
                            else {
                                outOfRangeFlag = true;
                            }
                        }
                        if ( outOfRangeFlag == true ) { // Now Price will be the last one
                            this.calculateFinalPrice = userUnit * qtyPriceData[qtyPriceData.length - 1].price;
                            this.TicketForm.unit_price = qtyPriceData[qtyPriceData.length - 1].price;
                        }
                    }
                    else {
                        this.calculateFinalPrice = 0;
                    }
                }
            },
            created(){
                this.GetLocations();
                this.GetDetails();
            }
        });
    </script>
@endsection